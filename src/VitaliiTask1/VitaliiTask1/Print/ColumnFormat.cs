﻿using System;
using VitaliiTask1.Infractucture;

namespace VitaliiTask1.Print
{
    public class ColumnFormat : IFormat
    {

        public string Separator { get; }
        public int NumberColumn { get; }

        public ColumnFormat(string separator, int numberColumn)
        {
            this.Separator = separator;
            this.NumberColumn = numberColumn;
        }

        public string Format(double[] value)
        {
            string[] newValue = getNewValue(value);

            return string.Join(Separator, newValue);
        }

        private string[] getNewValue(double[] result)
        {
            string[] arrResult = new string[result.Length];

            for (int i = 0; i < result.Length; i++)
            {
                if ((i + 1) % NumberColumn != 0)
                {
                    arrResult[i] = result[i] + "\t";
                }
                else
                {
                    arrResult[i] = result[i] + "\n";
                }
            }

            return arrResult;
        }

    }
}
