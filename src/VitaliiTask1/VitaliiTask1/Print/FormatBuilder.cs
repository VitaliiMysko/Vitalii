﻿using System;
using VitaliiTask1.Infractucture;

namespace VitaliiTask1.Print
{
    class FormatBuilder
    {
        private int numberColumn;

        private bool toLine;
        private bool toColumn;

        private byte countFormat;

        public IFormat Build()
        {

            if (this.countFormat > 1)
            {
                throw new Exception($"command is not correct");
            }

            if (toLine)
            {
                return new SimpleFormat(",");
            }

            if (toColumn)
            {
                return new ColumnFormat("", numberColumn);
            }

            return new SimpleFormat("\n");

        }

        public void SetInLine()
        {
            toLine = true;
            countFormat++;
        }

        public void SetToCol(int num)
        {
            numberColumn = num;
            toColumn = true;
            countFormat++;
        }

    }
}