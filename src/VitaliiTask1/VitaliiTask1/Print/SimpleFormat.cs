﻿using VitaliiTask1.Infractucture;

namespace VitaliiTask1.Print
{
    public class SimpleFormat : IFormat
    {

        public string Separator { get; }

        public SimpleFormat(string separator)
        {
            this.Separator = separator;
        }

        public string Format(double[] value)
        {
            return string.Join(Separator, value);
        }

    }
}
