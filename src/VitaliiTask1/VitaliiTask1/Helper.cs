﻿using System;

namespace VitaliiTask1
{
    public class Helper
    {

        public int ParseStrToNum(string value)
        {

            if (!int.TryParse(value, out int num))
            {
                throw new Exception($"the number \"{value}\" is not correct");
            }

            return num;

        }

    }
}
