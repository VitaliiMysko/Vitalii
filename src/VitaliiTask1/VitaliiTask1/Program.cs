﻿using Microsoft.Extensions.CommandLineUtils;
using System;
using VitaliiTask1.Infractucture;
using VitaliiTask1.Commands;
using VitaliiTask1.Print;
using VitaliiTask1.Outputs;

namespace VitaliiTask1
{
    class Program
    {

        static void Main(string[] args)
        {

            Helper helper = new Helper();

            CommandLineApplication app =
              new CommandLineApplication(throwOnUnexpectedArg: false);

            app.Name = "сonsole application v1.0.";
            app.Description = ".NET Core сonsole application v1.0. with argument parsing.";

            var optToLine = app.Option("-l", "Print result in line", CommandOptionType.NoValue);
            var optToColumn = app.Option("-c <number>", "Print result in <number> columns", CommandOptionType.SingleValue);
            var optToFile = app.Option("-o | --output <path>", "Output result to file", CommandOptionType.SingleValue);
            var optToDisplay = app.Option("-d", "Output result of file on display", CommandOptionType.NoValue);

            app.HelpOption("-? | -h | --help");


            app.OnExecute(() =>
            {
                return 0;
            });

            app.Command("fib", (command) =>
            {
                command.Description = "FIBONACCI NUMBERS";
                var argNumber = command.Argument("number", "displays the number of elements from Fibonacci numbers");

                command.OnExecute(() =>
                {
                    string value = argNumber.Value;

                    if (string.IsNullOrEmpty(value))
                    {
                        command.ShowHelp();
                        return 0;
                    }

                    var formatBuilder = new FormatBuilder();
                    if (optToLine.HasValue())
                    {
                        formatBuilder.SetInLine();
                    }

                    if (optToColumn.HasValue())
                    {
                        int numberColumn = new Helper().ParseStrToNum(optToColumn.Value());
                        formatBuilder.SetToCol(numberColumn);
                    }
                    IFormat format = formatBuilder.Build();


                    var outputBuilder = new OutputBuilder();

                    if (optToFile.HasValue())
                    {
                        string nameFile = optToFile.Value();
                        outputBuilder.SetToFile(nameFile);

                        if (optToDisplay.HasValue())
                        {
                            outputBuilder.SetToDisplay();
                        }

                    }
                    IOutput output = outputBuilder.Build();


                    int num = helper.ParseStrToNum(value);

                    ICommand c = new FibonacciCommand(num);
                    RunCommand(format, output, c);


                    return 0;
                });

            });

            app.Command("ferma", (command) =>
            {
                command.Description = "FERMA NUMBERS";
                var argNumber = command.Argument("number", "displays the number of elements from Ferma numbers");

                command.OnExecute(() =>
                {
                    string value = argNumber.Value;

                    if (string.IsNullOrEmpty(value))
                    {
                        command.ShowHelp();
                        return 0;
                    }

                    var formatBuilder = new FormatBuilder();
                    if (optToLine.HasValue())
                    {
                        formatBuilder.SetInLine();
                    }

                    if (optToColumn.HasValue())
                    {
                        int numberColumn = new Helper().ParseStrToNum(optToColumn.Value());
                        formatBuilder.SetToCol(numberColumn);
                    }
                    IFormat format = formatBuilder.Build();


                    var outputBuilder = new OutputBuilder();

                    if (optToFile.HasValue())
                    {
                        string nameFile = optToFile.Value();
                        outputBuilder.SetToFile(nameFile);

                        if (optToDisplay.HasValue())
                        {
                            outputBuilder.SetToDisplay();
                        }

                    }
                    IOutput output = outputBuilder.Build();


                    int num = helper.ParseStrToNum(value);

                    ICommand c = new FermaCommand(num);
                    RunCommand(format, output, c);

                    return 0;
                });

            });


            try
            {
                app.Execute(args);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            };

        }

        private static void RunCommand(IFormat format, IOutput output, ICommand c)
        {
            double[] result = c.Execute();
            output.Write(format.Format(result));
        }

        private static IOutput CreateOutput()
        {
            return new ConsoleOutput();
        }

    }

}
