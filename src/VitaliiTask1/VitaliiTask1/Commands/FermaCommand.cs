﻿using System;
using VitaliiTask1.Infractucture;


namespace VitaliiTask1.Commands
{
    public class FermaCommand : ICommand
    {
        public int Num { get; }

        public FermaCommand(int num)
        {
            Num = num;
        }

        public double[] Execute()
        {
            double[] ferma = new double[Num];

            for (int i = 0; i < Num; i++)
            {

                double pow = Math.Pow(2, i);
                ferma[i] = Math.Pow(2, pow) + 1;

            }

            return ferma;

        }

    }
}
