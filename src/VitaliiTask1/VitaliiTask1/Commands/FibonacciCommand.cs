﻿using System;
using VitaliiTask1.Infractucture;

namespace VitaliiTask1.Commands
{
    public class FibonacciCommand : ICommand
    {

        public int Num { get; set; }

        public FibonacciCommand(int num)
        {
            Num = num;
        }


        public double[] Execute()
        {
            bool negativeNum = false;

            if (Num < 0)
            {
                Num *= (-1);
                negativeNum = true;
            }

            double[] fib = new double[Num];

            for (int i = 0; i < Num; i++)
            {

                if (i < 2)
                {
                    fib[i] = negativeNum ? -i : i;
                }
                else
                {
                    double beforeLastNum = fib[i - 2];
                    double lastNum = fib[i - 1];
                    fib[i] = beforeLastNum + lastNum;
                }

            }

            return fib;
        }

    }
}
