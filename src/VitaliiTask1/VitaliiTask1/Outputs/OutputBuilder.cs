﻿using VitaliiTask1.Infractucture;

namespace VitaliiTask1.Outputs
{
    class OutputBuilder
    {
        private string NameFile;
        private bool ToDisplay;

        public IOutput Build()
        {

            if (!string.IsNullOrEmpty(NameFile))
            {

                IOutput output = new NotifyOutputComplitionDecorator(new FileOutput(NameFile), NameFile);

                if (ToDisplay)
                {
                    output = new NotifyOutputToDisplayComplitionDecorator(output);
                }

                return output;

            }

            return new ConsoleOutput();

        }

        public void SetToFile(string nameFile)
        {
            this.NameFile = nameFile.Trim();
        }

        public void SetToDisplay()
        {
            this.ToDisplay = true;
        }

    }
}