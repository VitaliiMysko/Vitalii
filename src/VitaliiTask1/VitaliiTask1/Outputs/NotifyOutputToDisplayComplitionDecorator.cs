﻿using System;
using System.IO;
using VitaliiTask1.Infractucture;

namespace VitaliiTask1.Outputs
{
    class NotifyOutputToDisplayComplitionDecorator : IOutput
    {
        protected IOutput Output { get; }

        public NotifyOutputToDisplayComplitionDecorator(IOutput output)
        {
            if (output == null)
            {
                throw new Exception("NotifyOutputComplitionDecorator | No identification class for inreacing functionality");
            }

            this.Output = output;
        }

        public void Write(string value)
        {

            Output.Write(value);

            IOutput output = new ConsoleOutput();
            output.Write(value);

        }
    }
}
