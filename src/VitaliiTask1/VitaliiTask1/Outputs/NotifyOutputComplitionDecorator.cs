﻿using System;
using System.IO;
using VitaliiTask1.Infractucture;

namespace VitaliiTask1.Outputs
{
    class NotifyOutputComplitionDecorator : IOutput
    {
        protected IOutput Output { get; }
        private string NameFile { get; }
        public NotifyOutputComplitionDecorator(IOutput output, string nameFile)
        {
            if (output == null)
            {
                throw new Exception("NotifyOutputComplitionDecorator | No identification class for inreacing functionality");
            }

            this.Output = output;
            this.NameFile = nameFile;

        }

        public void Write(string value)
        {

            Output.Write(value);

            IOutput output = new ConsoleOutput();
            output.Write("file saved successfully => " + Path.Combine(Directory.GetCurrentDirectory(), NameFile));

        }
    }
}
