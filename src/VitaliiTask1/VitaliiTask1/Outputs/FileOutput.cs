﻿using System;
using System.IO;
using VitaliiTask1.Infractucture;

namespace VitaliiTask1.Outputs
{
    class FileOutput : IOutput
    {

        private string NameFile { get; }
        public string FullPath
        {
            get
            {
                return Path.Combine(Directory.GetCurrentDirectory(), NameFile);
            }
        }

        public FileOutput(string namefile)
        {
            NameFile = namefile;
        }

        public void Write(string value)
        {

            if (!File.Exists(FullPath))
            {
                throw new Exception($"File \"{NameFile}\" not find");
            }

            using (StreamWriter outputFile = new StreamWriter(FullPath))

                outputFile.WriteLine(value);

        }
    }
}