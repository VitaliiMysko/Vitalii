﻿using System;
using VitaliiTask1.Infractucture;

namespace VitaliiTask1.Outputs
{
    class ConsoleOutput : IOutput
    {
        public void Write(string value)
        {
            Console.WriteLine(value);
        }

    }
}